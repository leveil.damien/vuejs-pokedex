# Objectifs : 
A partir des ressources disponibles dans le dossier `data`, implémentez une application permettant d'afficher un pokedex. 

## Consignes

Au sein de ce dossier vous trouverez un fichier CSV contenant de nombreuses informations. 
> This dataset contains information on all 802 Pokemon from all Seven Generations of Pokemon. The information contained in this dataset include Base Stats, Performance against > Other Types, Height, Weight, Classification, Egg Steps, Experience Points, Abilities, etc. The information was scraped from http://serebii.net/

Ce fichier source est issu de Kaggle. A partir du moment où il s'agit d'un fichier plat, 
Vous pouvez télécharger d'autres sources de données. Il vous est également possible de converir cette structure de données vers un autre format plus aproprié (json par exemple).

Réalisez un site web permettant de : 
 - lister l'ensemble des pokemons
 - visualiser un pokemon ( vous disposez également d'un fichier image dans `data/images`, il se peut qu'une image manque, vous mettrez une [image place holder](https://placehold.co/) à la place.)
 - Composer un deck de 5 pokémons

Une attention toute particulière est attendue pour le graphisme de votre site ainsi que de la mise en valeur des informations. N'hésitez par à utiliser des assets ou encore des frameworks CSS tels que bootstrap ou tailwind.

Un utilisateur aura un deck et aura la possibilité de rajouter un pokemon (à la limite de 5) dans son deck.
Finalement, une page arène devra être implémentée.
La page arène est à destinatation d'un lieu d'affrontement entre les pokémons. 

Au sein de cette page arène, l'utilisateur pourra voir une liste de 5 pokémons aléatoirements définis issus du pokedex, ainsi que les 5 pokémons présents au sein de son deck. 

La gestion du combat entre les pokémons n'est pas à implémenter.

Le page flow est donc le suivant : 

```mermaid
flowchart TD
    A[Pokedex]
    A <-->|Add/Remove to deck| A
    A <--> G[View pokemon]
    A <--> E[View deck]
    A <--> I[View arena]
```

## Consignes /Livraison
 - Ce projet peut être réalisé seul ou en binôme
 - Ce projet devra être livré via un dépôt git
 - Il est attendu de livrer : 
    - Une maquette
    - Un projet abouti correspondant aux maquettes
 - Attention aux nommages de variables et des méthodes. 
 - Attention aux nommages des routes, elles doivent être cohérentes
 - Attention à avoir une décomposition des composants cohérente
 - Il ne doit y avoir aucun console.log ou code commenté futile